yii2-doc
======================
auto generate api document by using comment for yii2.

Installation
======================

```
composer require jiang704593835/yii2-doc2
```

Configure
======================

```php
    'module'=>[
        'doc' => [
            'class' => 'cfd\doc\Module',
            'modelDescriptions'=>require __DIR__.'/model_description.php',
            'modelsMap'=>[
                '\common\models\\',
            ]
        ]
    ]
```

Use1
======================

```php
/**
 * @doc-module 登录
 */
class UserController extends Controller {
```

doc-param 类型 字段 描述 默认值 是否必须

doc-return 类型 返回定义 描述


```php
/**
 * @doc-name 用户详情
 * @doc-param string username 用户名称 / optional
 * @doc-desc 获取用户详情
 * @doc-return mixed @List{@User-mixed-用户信息{username-string-用户名,mobile},name-string-姓名,age-int-年龄,@Activity{*[id|title],@ActivityData{*}}} 用户详情
 * @doc-return mixed @Pagination{per_page-string-分页} 分页数据
 * @doc-return string name 名称
 */
public function actionDetail()
```




