<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;

\cfd\doc\DocAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="none" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        /* 布局 */
        .ui{
            height: 98%;

        }
        .site-tree, .site-content {

            display: inline-block;
            *display: inline;
            *zoom: 1;
            vertical-align: top;
            font-size: 14px;
        }

        .site-tree {
            float: left;
            width: 325px;
            height: 100%;
            padding: 5px 0 20px;
            overflow: scroll;
        }

        .site-content {
            width: calc(100% - 325px);
            height: 100%;
            padding: 20px 0 10px 20px;
            overflow: scroll;

        }

        /* 文档 */
        .site-tree {
            border-right: 1px solid #eee;
        }

        .site-tree .layui-tree {
            list-style: none;
            line-height: 32px;
        }

        .site-tree .layui-tree li i {
            position: relative;
            font-size: 22px;
            color: #000
        }

        .site-tree .layui-tree li a cite {
            padding: 0 8px;
        }

        .site-tree .layui-tree .site-tree-noicon a cite {
            padding-left: 15px;
        }

        .site-tree .layui-tree li a em {
            font-size: 12px;
            color: #bbb;
            padding-right: 5px;
            font-style: normal;
        }

        .site-tree .layui-tree li h2 {
            line-height: 36px;
            border-left: 5px solid #009E94;
            margin: 15px 0 5px;
            padding: 0 10px;
            background-color: #f2f2f2;
        }

        .site-tree .layui-tree li ul {
            list-style: none;
            margin-left: 27px;
            line-height: 28px;
        }

        .site-tree .layui-tree li ul a,
        .site-tree .layui-tree li ul a i {
            color: #777;
        }

        .site-tree .layui-tree li ul a:hover {
            color: #333;
        }

        .site-tree .layui-tree li ul li {
            margin-left: 25px;
            overflow: visible;
            list-style-type: disc; /*list-style-position: inside;*/
        }

        .site-tree .layui-tree li ul li cite,
        .site-tree .layui-tree .site-tree-noicon ul li cite {
            padding-left: 0;
        }

        .site-tree .layui-tree .layui-this a {
            color: #01AAED;
        }

        .site-tree .layui-tree .layui-this .layui-icon {
            color: #01AAED;
        }

        .site-fix .site-tree {
            position: fixed;
            top: 0;
            bottom: 0;
            z-index: 666;
            min-height: 0;
            overflow: auto;
            background-color: #fff;
        }

        .site-fix .site-content {
            margin-left: 220px;
        }

        .site-fix-footer .site-tree {
            margin-bottom: 120px;
        }

        .site-title {
            margin: 30px 0 20px;
        }

        .site-title fieldset {
            border: none;
            padding: 0;
            border-top: 1px solid #eee;
        }

        .site-title fieldset legend {
            margin-left: 20px;
            padding: 0 10px;
            font-size: 22px;
            font-weight: 300;
        }

        .site-text a {
            color: #01AAED;
        }

        .site-h1 {
            margin-bottom: 20px;
            line-height: 60px;
            padding-bottom: 10px;
            color: #393D49;
            border-bottom: 1px solid #eee;
            font-size: 28px;
            font-weight: 300;
        }

        .site-h1 .layui-icon {
            position: relative;
            top: 5px;
            font-size: 50px;
            margin-right: 10px;
        }

        .site-text {
            position: relative;
        }

        .site-text p {
            margin-bottom: 10px;
            line-height: 22px;
        }

        .site-text em {
            padding: 0 3px;
            font-weight: 500;
            font-style: italic;
            color: #666;
        }

        .site-text code {
            margin: 0 5px;
            padding: 3px 10px;
            border: 1px solid #e2e2e2;
            background-color: #fbfbfb;
            color: #666;
            border-radius: 2px;
        }

        .site-table {
            width: 100%;
            margin: 10px 0;
        }

        .site-table thead {
            background-color: #f2f2f2;
        }

        .site-table th,
        .site-table td {
            padding: 6px 15px;
            min-height: 20px;
            line-height: 20px;
            border: 1px solid #ddd;
            font-size: 14px;
            font-weight: 400;
        }

        .site-table tr:nth-child(even) {
            background: #fbfbfb;
        }

        .site-block {
            padding: 20px;
            border: 1px solid #eee;
        }

        .site-block .layui-form {
            margin-right: 200px;
        }
        .td_type {
            display: block;
            width: 400px;
            overflow: hidden;
        }
    </style>

</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
